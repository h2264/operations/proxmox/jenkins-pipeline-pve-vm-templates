.PHONY: venv_init all precheck download build clean

_VM=time vagrant
_VENV=.venv
_EVARS=-e ansible_ssh_private_key_file=~/.ssh/customkeys/pve
_INVENTORY=./inventories/pve.yaml
_PLAYBOOK=templates.yaml

all: precheck download build clean
	
precheck:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=precheck $(_PLAYBOOK))

download:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=download $(_PLAYBOOK))

build:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=build $(_PLAYBOOK))

clean:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=cleanup $(_PLAYBOOK))

venv_init:
	$(call venv_exec,$(_VENV),pip install --upgrade pip)
	$(call venv_exec,$(_VENV),pip install -r requirements.txt)

# VENV FUNCTIONS
define venv_exec
	$(if [ ! -f "$($(1)/bin/activate)" ], python3 -m venv $(1))
	( \
    	source $(1)/bin/activate; \
    	$(2) \
	)
endef
