.PHONY: download create import attach \
	cdrom bootorder console template

download:
# download the image
	wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img

create:
# create a new VM
	qm create 9000 --memory 2048 --net0 virtio,bridge=vmbr0

import:
# import the downloaded disk to local-lvm storage
	qm importdisk 9000 bionic-server-cloudimg-amd64.img local-lvm

attach:
# finally attach the new disk to the VM as scsi drive
	qm set 9000 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-9000-disk-1

cdrom:
	qm set 9000 --ide2 local-lvm:cloudinit

bootorder:
	qm set 9000 --boot c --bootdisk scsi0

console:
	qm set 9000 --serial0 socket --vga serial0

template:
