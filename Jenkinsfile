def taglist = []
def updatedimages = []

pipeline {
  agent {
    node {
      label 'drone'
    }
  }

  environment {
    INVENTORY = 'inventories/pve.yaml'
    PIPELINE_PVE_VM_E2E = 'provisioning/pipeline-pve-vm-e2e-deployment-test'
    SSHKEY_PROXMOX = 'VAULT_JENKINS_PROXMOX_SSHKEY'
  }

  options {
    ansiColor('gnome-terminal')
    disableConcurrentBuilds()
    buildDiscarder(logRotator(daysToKeepStr: '7', numToKeepStr: '20'))
  }

  parameters {
    choice choices: ['templates.yaml'], name: 'PLAYBOOK'
    string(
        name: 'SLACK_CHANNEL',
        defaultValue: 'jenkins-pipeline-pve-vm-templates',
        description: 'Slack channel to receive notifications'
    )
    credentials credentialType: 'com.datapipe.jenkins.vault.credentials.common.VaultSSHUserPrivateKeyImpl',
      defaultValue: 'VAULT_JENKINS_GITLAB_SSHKEY',
      name: 'GIT_CREDS', 
      required: true
  }

  stages {
    stage('Create List') {
      steps {
          script {
              taglist = ["precheck","download","build","cleanup"]
              repolist = [
                "template-fedora-server-cloudimg-amd64.img" : "git@gitlab.com:h2264/operations/proxmox/tf-pve-template-vm-fedora.git",
                "template-focal-server-cloudimg-amd64.img" : "git@gitlab.com:h2264/operations/proxmox/tf-pve-template-vm-focal.git"
              ]
          }
      }
    }

    stage('Dynamic Stages') {
      steps {
        script {
          for(int i=0; i < taglist.size(); i++) {
            stage("TAG: ${taglist[i]}"){
              catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                ansiblePlaybook become: true, 
                  colorized: true, 
                  credentialsId: SSHKEY_PROXMOX, 
                  disableHostKeyChecking: true, 
                  installation: 'virtualenv',
                  inventory: INVENTORY,
                  playbook: PLAYBOOK,
                  tags: taglist[i]
              }
            }
          }
        }
      }
    }

    stage ("Deployment test") {
      steps{
        script {
          _GIT_FILE_LIST = sh(
            script: "git diff-files --name-only",
            returnStdout: true
          )
          if (_GIT_FILE_LIST) {
            for (_FILE_RAW in _GIT_FILE_LIST.split("\n")) {
              def _FILE    = _FILE_RAW.replace('files/','')
              def _REPO    = repolist[_FILE]
              def _CHANNEL = params.SLACK_CHANNEL
              println("_FILE: ${_FILE}")
              println("GIT_REPO: ${_REPO}")
              println("DOWNSTREAM: ${PIPELINE_PVE_VM_E2E}")
              slackSend channel: params.SLACK_CHANNEL,
                message: """
                *${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}
                UPDATED IMAGE DETECTED: ${_FILE}
                INVOKING DOWNSTREAM PIPELINE: ${PIPELINE_PVE_VM_E2E}
                """
              build job: PIPELINE_PVE_VM_E2E, 
                parameters: [
                  string(
                    name: 'GIT_REPO', 
                    value: _REPO
                  ),
                  string(
                    name: '_SLACK_CHANNEL', 
                    value: params.SLACK_CHANNEL
                  )
                ],
                propagate: true,
                quietPeriod: 1,
                wait: true
            }
          }
        }
      }
    }

    stage ("Template complete! Update SCM") {
      environment {
        GIT_SSH_COMMAND = "ssh -o StrictHostKeyChecking=no"
      }
      steps {
        script {
            sshagent([GIT_CREDS.toString()]) {
            _GIT_FILE_LIST = sh(
              script: "git diff-files --name-only",
              returnStdout: true
            )
            if (_GIT_FILE_LIST) {
              for (_FILE in _GIT_FILE_LIST.split("\n")) {
                sh("git add ${_FILE}")
              }
              sh("git commit -m 'BUILD: ${env.BUILD_NUMBER}'")
              sh("GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no' git push origin HEAD:main")
            }
          }
        }
      }
    }
  }

  post {
    always {
      script {
        BUILD_USER = getBuildUser()
        COLOR_MAP = [
          'SUCCESS': 'good',
          'FAILURE': 'danger',
        ]
      }
      slackSend channel: params.SLACK_CHANNEL,
          color: COLOR_MAP[currentBuild.currentResult],
          message: """
          *${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}
          GIT_URL: ${env.GIT_URL}
          GIT_BRANCH: ${env.GIT_BRANCH}
          GIT_COMMIT: ${env.GIT_COMMIT}
          More info at: ${env.BUILD_URL}
          """
    }

    cleanup {
      cleanWs()
    }
  }

  triggers {
    cron '0 */6 * * *'
  }
}

String getBuildUser() {
  ID = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause').userId
  if (ID) {
    return ID
  }
  else {
    return '[N/A. Launched by cronTrigger: * */6 * *]'
  }
}